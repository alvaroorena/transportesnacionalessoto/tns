<?php

namespace app\controllers;

use app\models\Cargas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Camiones;
use kartik\mpdf\Pdf;
use app\models\Naves;
use SqlDataProvider;
use Yii;


/**
 * CargasController implements the CRUD actions for Cargas model.
 */
class CargasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Cargas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Cargas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_carga' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cargas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionViewpdf($id)
    {
        $content = $this->renderPartial('vistapdf', [
            'model' => $this->findModel($id),
        ]);
        $albaran = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE, 
        // A4 paper format
        'format' => Pdf::FORMAT_A4, 
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT, 
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER, 
        // your html content input
        'content' => $content,  
        'cssInline' => '.kv-heading-1{font-size:18px}', 
         // set mPDF properties on the fly
        'options' => ['title' => 'Albarán'],
         // call mPDF methods on the fly
        'methods' => [ 
            'SetHeader'=>['Transportes Nacionales Soto'], 
            'SetFooter'=>['{PAGENO}'],
        ]
    ]);
        return $albaran->render();
    }

    /**
     * Creates a new Cargas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//               
//        $model = new Cargas();
//        
//        $modelcamiones = Camiones::find()
////                -> InnerJoinWith(['naves'])
//                ->where("estado='Disponible'")
//                ->orderBy ('modelo_completo')
//                ->all();
//       
//            if ($this->request->isPost) {
//            if ($model->load ($this->request->post()) && $model->save()) {
//                $camion=Camiones::findOne($model->codigo_camion);
//                $camion->estado="Trabajando";
//                $camion->save();
//                return $this->redirect(['view', 'id' => $model->codigo_carga]);
//            }
//         } else {
//            $model->loadDefaultValues();
//        }
//        
//        
//            
//
//        return $this->render('create', [
//            'model' => $model,
//            'modelcamiones' => $modelcamiones,
//            
//        ]);
//    }
    
//    public function actionCreate()
//    {
//               
//        $model = new Cargas();
//        
//        $modelcamiones = Camiones::find()
////                -> InnerJoinWith(['naves'])
//                ->where("estado='Disponible'")
//                ->orderBy ('modelo_completo')
//                ->all();
//       
//            if ($this->request->isPost) {
//            if ($model->load ($modelcamiones = Camiones::findOne()->estado='Trabajando') ($this->request->post()) && $model->save()) {
//                return $this->redirect(['view', 'id' => $model->codigo_carga]);
//            }
//         } else {
//            $model->loadDefaultValues();
//        }
//        
//        
//            
//
//        return $this->render('create', [
//            'model' => $model,
//            'modelcamiones' => $modelcamiones,
//            
//        ]);
//    }

     public function actionCreate(){
        
      
        $model = new Cargas();
        $modelcamiones = Yii::$app->db
                ->createCommand('SELECT c.*,n.ubicacion ubicacion FROM camiones c INNER JOIN naves n USING (codigo_nave) WHERE c.estado = "Disponible"')
                ->queryAll();
        
       if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                 $camion=Camiones::findOne($model->codigo_camion);
                  $camion->estado="Trabajando";
                  $camion->save();
                return $this->redirect(['view', 'id' => $model->codigo_carga]);
            }
         } else {
            $model->loadDefaultValues();
        }
        
        
            

        return $this->render('create', [
            'model' => $model,
            'modelcamiones' => $modelcamiones,
        ]);
        
        
    }
//    public function actionCreate2()
//    {
//        $model = new Cargas();
//        $modelcamioness = Camiones::find()
//                ->where("estado='Disponible'")
//                ->orderBy ('nave_origen')
//                ->all();
//       
//            if ($this->request->isPost) {
//            if ($model->load($this->request->post()) && $model->save()) {
//                return $this->redirect(['view', 'id' => $model->codigo_carga]);
//            }
//         } else {
//            $model->loadDefaultValues();
//        }
//        
//        
//            
//
//        return $this->render('create', [
//            'model' => $model,
//            'modelcamiones' => $modelcamioness,
//        ]);
//    }
    
//    public function actionCreate()
//    {
//        $model = new Cargas();
//        $modelcamiones = Camiones::find()
//                ->innerJoinWith('naves','camiones.codigo_nave = naves.codigonave')
//                ->innerJoinWith('cargas','camiones.codigo_camion = cargas.codigocamion')
//                ->where("naves.ubicacion = c1.empresaorigen")
//                ->orderBy ('modelo_completo')
//                ->all();
//       
//            if ($this->request->isPost) {
//            if ($model->load($this->request->post()) && $model->save()) {
//                return $this->redirect(['view', 'id' => $model->codigo_carga]);
//            }
//         } else {
//            $model->loadDefaultValues();
//        }
//        
//        
//            
//
//        return $this->render('create', [
//            'model' => $model,
//            'modelcamiones' => $modelcamiones,
//        ]);
//    }
//    public function actionCreate(){
//       
//      
//        $model = new Cargas();
//        $modelcamiones = new \yii\data\SqlDataProvider([
//           
//           'QueryAll'=>'SELECT * FROM camiones c INNER JOIN naves n ON c.codigo_nave = n.codigo_nave INNER JOIN cargas c2 ON c.codigo_camion = c2.codigo_camion WHERE n.ubicacion = c2.empresa_origen;',
//           
//       
//            ]);
//        
//        if ($this->request->isPost) {
//            if ($model->load($this->request->post()) && $model->save()) {
//                return $this->redirect(['view', 'id' => $model->codigo_carga]);
//            }
//         } else {
//           $model->loadDefaultValues();
//        }
//        
//       return $this->render('create', [
//            'model' => $model,
//            'modelcamiones' => $modelcamiones,
//       ]);
//        
//        
//    }
    
    
    /**
     * Updates an existing Cargas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelcamiones = Camiones::find()
                ->orderBy ('modelo_completo')
                ->all();
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_carga]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelcamiones' => $modelcamiones,
        ]);
    }

    /**
     * Deletes an existing Cargas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cargas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cargas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cargas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    
    public function actionPdf($codigo) {
    //Consulta remeros
    $cargas= new ActiveDataProvider([
           'query'=> Cargas::find()
                ->select("codigo_carga,descripcion_carga,peso")
                ->where("codigo_carga='$codigo'"),
        ]);
    
    //Consulta entrenadores    
//    $entrenadores= new ActiveDataProvider([
//       'query'=>Entrenadores::find()->select('nombre_completo,dni')
//            ->where("codigo_categoria='$codigo'")
//    ]);
    // Renderizo los datros en la vista de remeroscategoria
    $content = $this->renderPartial('cargaspdf',[
//            "entrenadores" => $entrenadores,
//            "campos_entrenadores" => ['nombre_completo','dni'],
            "titulo_entrenadores" => "El entrenador de la categoría ".$codigo." es: ",
            "remeros"=>$cargas,
            "campos_carga"=>['codigo_carga','descripcion','peso'],
            "titulo_remeros"=>"Los remeros que la componen son:",
            "codigo" => $codigo,
    ]);
    // setup kartik\mpdf\Pdf component
    $remerosporcategoria = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE, 
        // A4 paper format
        'format' => Pdf::FORMAT_A4, 
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT, 
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER, 
        // your html content input
        'content' => $content,  
        'cssInline' => '.kv-heading-1{font-size:18px}', 
         // set mPDF properties on the fly
        'options' => ['title' => 'Krajee Report Title'],
         // call mPDF methods on the fly
        'methods' => [ 
            'SetHeader'=>['Rowwwing APP'], 
            'SetFooter'=>['{PAGENO}'],
        ]
    ]);
    
    return $albaranes->render(); 

    }
    
        }
