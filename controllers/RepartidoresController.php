<?php

namespace app\controllers;

use app\models\Repartidores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Furgonetas;

/**
 * RepartidoresController implements the CRUD actions for Repartidores model.
 */
class RepartidoresController extends Controller
{
    /**
     * @inheritDoc
     */
  
    public function actionConsultadisponibilidadrepartidores(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT r.nombre_completo, n.ubicacion, r.salario_kilometro FROM furgonetas f INNER JOIN repartidores r ON f.codigo_furgoneta = r.codigo_furgoneta INNER JOIN naves n ON f.codigo_nave = n.codigo_nave WHERE r.estado = "Disponible" ',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        $dataProvider->setSort([
        'attributes' => [
            'nombre_completo',
            'ubicacion',
            'ubicacion',
            'salario_kilometro',
            'lastReportResult' => [
                'asc' => ['lastReportResult' =>SORT_ASC ],
                'desc' => ['lastReportResult' => SORT_DESC],
                'default' => SORT_ASC
            ],
            'reportPercentDiff'
        ]
    ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nombre_completo', 'ubicacion','salario_kilometro' ],
           "titulo"=>"Repartidores Disponibles",
           "enunciado"=>" ",
           "sql"=>" "
       ]);
        
        
    }
    
    
    public function actionConsultadisponibilidadrepartidores2(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT r.nombre_completo, n.ubicacion, r.salario_kilometro FROM furgonetas f INNER JOIN repartidores r ON f.codigo_furgoneta = r.codigo_furgoneta INNER JOIN naves n ON f.codigo_nave = n.codigo_nave WHERE r.estado = "Trabajando" ORDER BY r.salario_kilometro',
           'pagination'=>[
               'pageSize' =>5,
           ],
            
       ]);
        $dataProvider->setSort([
        'attributes' => [
            'nombre_completo',
            'ubicacion',
            'ubicacion',
            'salario_kilometro',
            'lastReportResult' => [
                'asc' => ['lastReportResult' =>SORT_ASC ],
                'desc' => ['lastReportResult' => SORT_DESC],
                'default' => SORT_ASC
            ],
            'reportPercentDiff'
        ]
    ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nombre_completo', 'ubicacion','salario_kilometro' ],
           "titulo"=>"Repartidores Trabajando",
           "enunciado"=>" ",
           "sql"=>" "
       ]);
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Repartidores models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Repartidores::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_repartidor' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Repartidores model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Repartidores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Repartidores();
        $modelfurgonetas = Furgonetas::find()
                ->orderBy ('modelo_completo')
                ->all();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->codigo_repartidor]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'modelfurgonetas' => $modelfurgonetas,
        ]);
    }

    /**
     * Updates an existing Repartidores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelfurgonetas = Furgonetas::find()
                ->orderBy ('modelo_completo')
                ->all();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_repartidor]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelfurgonetas' => $modelfurgonetas,
        ]);
    }

    /**
     * Deletes an existing Repartidores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Repartidores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Repartidores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Repartidores::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
