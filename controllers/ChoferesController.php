<?php

namespace app\controllers;

use app\models\Choferes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Camiones;

/**
 * ChoferesController implements the CRUD actions for Choferes model.
 */
class ChoferesController extends Controller
{
    /**
     * @inheritDoc
     */
   
    public function actionConsultadisponibilidadchoferes(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT c1.nombre_completo,n.ubicacion,c1.salario_kilometro FROM camiones c INNER JOIN choferes c1 ON c.codigo_camion = c1.codigo_camion INNER JOIN naves n ON c.codigo_nave = n.codigo_nave WHERE c1.estado = "Disponible" ',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        $dataProvider->setSort([
        'attributes' => [
            'nombre_completo',
            'ubicacion',
            'ubicacion',
            'salario_kilometro',
            'lastReportResult' => [
                'asc' => ['lastReportResult' =>SORT_ASC ],
                'desc' => ['lastReportResult' => SORT_DESC],
                'default' => SORT_ASC
            ],
            'reportPercentDiff'
        ]
    ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nombre_completo', 'ubicacion','salario_kilometro' ],
           "titulo"=>"Chóferes Disponibles",
           "enunciado"=>" ",
           "sql"=>" "
       ]);
        
        
    }
    
    public function actionConsultadisponibilidadchoferes2(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT c1.nombre_completo,n.ubicacion,c1.salario_kilometro FROM camiones c INNER JOIN choferes c1 ON c.codigo_camion = c1.codigo_camion INNER JOIN naves n ON c.codigo_nave = n.codigo_nave WHERE c1.estado = "Trabajando"',
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        $dataProvider->setSort([
        'attributes' => [
            'nombre_completo',
            'ubicacion',
            'ubicacion',
            'salario_kilometro',
            'lastReportResult' => [
                'asc' => ['lastReportResult' =>SORT_ASC ],
                'desc' => ['lastReportResult' => SORT_DESC],
                'default' => SORT_ASC
            ],
            'reportPercentDiff'
        ]
    ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['nombre_completo', 'ubicacion','salario_kilometro' ],
           "titulo"=>"Chóferes Trabajando",
           "enunciado"=>" ",
           "sql"=>" "
       ]);
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Choferes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Choferes::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_chofer' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Choferes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Choferes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Choferes();
        $modelcamiones = Camiones::find()
                ->orderBy ('modelo_completo')
                 ->where("estado='Disponible'")
                ->all();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->codigo_chofer]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'modelcamiones' => $modelcamiones,
        ]);
    }

    /**
     * Updates an existing Choferes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelcamiones = Camiones::find()
                ->orderBy ('modelo_completo')
                ->all();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_chofer]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelcamiones' => $modelcamiones,
        ]);
    }

    /**
     * Deletes an existing Choferes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Choferes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Choferes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Choferes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
