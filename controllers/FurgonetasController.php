<?php

namespace app\controllers;

use app\models\Furgonetas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Naves;
/**
 * FurgonetasController implements the CRUD actions for Furgonetas model.
 */
class FurgonetasController extends Controller
{
    /**
     * @inheritDoc
     */
   
    
    public function actionConsultareparaciones(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Furgonetas::find()
                
                ->where("revision=0"),
           'pagination'=>[
               'pageSize' =>10,
           ]
       ]);
        
        
        return $this->render("resultadosrepara",[
           "resultadosrepara"=>$dataProvider,
           "campos"=>['matricula','modelo_completo','revision'],
           "titulo"=>"Mantenimiento Furgonetas",
           "enunciado"=>"Listado de furgonetas que necesitan de mantenimiento ",
           "sql"=>' '
       ]);
        
    }
    public function actionConsultakilometrajefurgonetas(){
        
      
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Furgonetas::find()
                
                ->distinct()
                ->orderBy("kilometros"),
           'pagination'=>[
               'pageSize' =>10,
           ]
       ]);
        
        
        return $this->render("resultadoskm",[
           "resultadoskm"=>$dataProvider,
           "campos"=>['matricula','modelo_completo', 'kilometros'],
           "titulo"=>"Kilometraje Furgonetas",
           "enunciado"=>"Listado de furgonetas ordenadas por su kilometraje ",
           "sql"=>' '
       ]);
        
        
    }
    
    
    public function actionConsultaconsumofurgonetas(){
        
      
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Furgonetas::find()
                
                ->distinct()
                ->orderBy("consumo"),
           'pagination'=>[
               'pageSize' =>10,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['matricula','modelo_completo', 'consumo'],
           "titulo"=>"Consumo Furgonetas",
           "enunciado"=>"Listado de furgonetas ordenadas por su consumo ",
           "sql"=>' '
       ]);
        
        
    }
    
    
    public function actionConsultadisponibilidadfurgonetas(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Furgonetas::find()
                
                ->where("estado='Disponible'"),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultadosdispo",[
           "resultadosdispo"=>$dataProvider,
           "campos"=>['matricula','modelo_completo'],
           "titulo"=>"Furgonetas Disponibles",
           "enunciado"=>"Listado de camiones que actualmente se encuentran disponibles  ",
           "sql"=>' '
       ]);
        
    }
    public function actionConsultadisponfur(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Furgonetas::find()
                
                ->where("estado='Trabajando'"),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['matricula','modelo_completo'],
           "titulo"=>"Furgonetas Trabajando",
           "enunciado"=>"Listado de furgonetas que actualmente se encuentran trabajando ",
           "sql"=>' '
       ]);
        
    }
    
    
    public function actionConsultadisponibilidadfurgonetas2(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Furgonetas::find()
                
                ->where("estado='Trabajando'"),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['matricula','modelo_completo'],
           "titulo"=>"Furgonetas Trabajando",
           "enunciado"=>" ",
           "sql"=>' '
       ]);
        
    }
    
    
    
    
    
    
    
    
    
    
    
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Furgonetas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Furgonetas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_furgoneta' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Furgonetas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Furgonetas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Furgonetas();
        $modelnaves = Naves::find()->all();
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->codigo_furgoneta]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'modelnaves' => $modelnaves,
        ]);
    }

    /**
     * Updates an existing Furgonetas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
         $modelnaves = Naves::find()->all();
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_furgoneta]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelnaves' => $modelnaves,
        ]);
    }
    
    public function actionUpdate1($id)
    {
        $model = $this->findModel($id);
         $modelnaves = Naves::find()->all();
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_furgoneta]);
        }

        return $this->render('update_1', [
            'model' => $model,
            'modelnaves' => $modelnaves,
        ]);
    }

    /**
     * Deletes an existing Furgonetas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Furgonetas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Furgonetas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Furgonetas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
