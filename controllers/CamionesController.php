<?php

namespace app\controllers;

use app\models\Camiones;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Naves;
use yii\helpers\Html;


/**
 * CamionesController implements the CRUD actions for Camiones model.
 */
class CamionesController extends Controller
{
    /**
     * @inheritDoc
     */
    
    
    
    /**
  Consulta para sacar los camiones que necesitan ser reparados
 */
    public function actionConsultareparaciones(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Camiones::find()
                
                ->where("revision=0"),
           'pagination'=>[
               'pageSize' =>10,
           ]
       ]);
        
        
        return $this->render("resultadosrepara",[
           "resultadosrepara"=>$dataProvider,
           "campos"=>['matricula','modelo_completo','revision'],
           "titulo"=>"Mantenimiento Camiones",
           "enunciado"=>"Listado de camiones que necesitan de mantenimiento ",
           "sql"=>' '
       ]);
        
    }
   
    public function actionConsultatest(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Camiones::find()
                
                ->where("estado='Disponible'"),
           'pagination'=>[
               'pageSize' =>10,
           ]
       ]);
        
        
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
          
           "campos"=>['matricula','modelo_completo','codigo_nave'],
           "titulo"=>"Camiones Disponibles",
           "enunciado"=>"Listado de camiones que actualmente se encuentra disponibles para formalizar cargas ",
           "sql"=>' '
       ]);
        
    }
    public function actionConsultadef(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT DISTINCT c.matricula, c.modelo_completo, n.ubicacion FROM naves n INNER JOIN camiones c USING(codigo_nave) WHERE c.estado = "Disponible" ',
           'pagination'=>[
               'pageSize' =>10,
           ]
       ]);
        
        $dataProvider->setSort([
        'attributes' => [
            'matricula',
            'modelo_completo',
            'ubicacion',
            
            'lastReportResult' => [
                'asc' => ['lastReportResult' =>SORT_ASC ],
                'desc' => ['lastReportResult' => SORT_DESC],
                'default' => SORT_ASC
            ],
            'reportPercentDiff'
        ]
    ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['matricula','modelo_completo','ubicacion' ],
           "titulo"=>"Camiones Disponibles",
           "enunciado"=>"Listado de camiones que actualmente se encuentran disponibles para formalizar cargas ",
           "sql"=>" "
       ]);
        
        
    }
    
    public function actionConsultadef2(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT DISTINCT c.matricula, c.modelo_completo, n.ubicacion FROM naves n INNER JOIN camiones c USING(codigo_nave) WHERE c.estado = "Trabajando" ',
           'pagination'=>[
               'pageSize' =>10,
           ]
       ]);
        
        $dataProvider->setSort([
        'attributes' => [
            'matricula',
            'modelo_completo',
            'ubicacion',
            
            'lastReportResult' => [
                'asc' => ['lastReportResult' =>SORT_ASC ],
                'desc' => ['lastReportResult' => SORT_DESC],
                'default' => SORT_ASC
            ],
            'reportPercentDiff'
        ]
    ]);
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['matricula','modelo_completo','ubicacion' ],
           "titulo"=>"Camiones Trabajando",
           "enunciado"=>"Listado de camiones que actualmente se encuentran trabajando ",
           "sql"=>" "
       ]);
        
        
    }
    
    
    
    public function actionConsultadispotest(){
        
      
        
        $dataProvider = new \yii\data\SqlDataProvider([
           
           'sql'=>'SELECT * FROM camiones WHERE estado = "Trabajando"',
           'pagination'=>[
               'pageSize' =>10,
           ]
       ]);
        
        
        
       return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['matricula','modelo_completo'],
           "titulo"=>"Camiones Trabajando",
           "enunciado"=>" ",
           "sql"=>" "
       ]);
        
        
    }
    public function actionConsultadisponibilidadcamiones1(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Camiones::find()
                
                ->where("estado='Disponible'"),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
            
           "resultados"=>$dataProvider,
           "campos"=>['matricula','modelo_completo','ubicacion'],
           "titulo"=>"Camiones Disponibles",
           "enunciado"=>" ",
           "sql"=>' '
       ]);
        
    }
    
    public function actionConsultadispo(){
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Camiones::find()
                
                ->where("estado='Trabajando'"),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['matricula','modelo_completo'],
           "titulo"=>"Camiones Trabajando",
           "enunciado"=>" ",
           "sql"=>' '
       ]);
        
    }
    
    
    
     public function actionConsultadisponibilidadcamiones2(){
        
      
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Camiones::find()
                ->select("matricula","modelo_completo")
                ->distinct()
                ->where("estado='Trabajando'"),
           'pagination'=>[
               'pageSize' =>5,
           ]
       ]);
        
        
        return $this->render("resultados",[
           "resultados"=>$dataProvider,
           "campos"=>['matricula','modelo_completo'],
           "titulo"=>"Camiones en Ruta",
           "enunciado"=>" ",
           "sql"=>' '
       ]);
        
        
    }
    /**
  Consulta para sacar el kilometraje de los camiones
 */
    public function actionConsultakilometrajecamiones(){
        
      
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Camiones::find()
                
                ->distinct()
                ->orderBy("kilometros"),
           'pagination'=>[
               'pageSize' =>10,
           ]
       ]);
        
        
        return $this->render("resultadoskm",[
           "resultadoskm"=>$dataProvider,
           "campos"=>['matricula','modelo_completo', 'kilometros'],
           "titulo"=>"Kilometraje Camiones",
           "enunciado"=>"Listado de camiones ordenados por su kilometraje ",
           "sql"=>' '
       ]);
        
        
    }
    /**
  Consulta para sacar el consumo de los camiones
 */
     public function actionConsultaconsumocamiones(){
        
      
        
        $dataProvider = new \yii\data\ActiveDataProvider([
           
           'query'=> \app\models\Camiones::find()
                
                ->distinct()
                ->orderBy("consumo"),
           'pagination'=>[
               'pageSize' =>10,
           ]
       ]);
        
        
        return $this->render("resultadosconsumo",[
           "resultadosconsumo"=>$dataProvider,
           "campos"=>['matricula','modelo_completo', 'consumo'],
           "titulo"=>"Consumo Camiones",
           "enunciado"=>"Listado de camiones ordenados por su consumo ",
           "sql"=>' '
       ]);
        
        
    }
    
    
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Camiones models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Camiones::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_camion' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
     public function actionBusquedas()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Camiones::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo_camion' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('busquedas', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Camiones model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Camiones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Camiones();
        
        $modelnaves = Naves::find()->all();

            if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->codigo_camion]);
            }
         } else {
            $model->loadDefaultValues();
        }
            

        return $this->render('create', [
            'model' => $model,
            'modelnaves' => $modelnaves,
        ]);
    }
    
    /**
     * Updates an existing Camiones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $modelnaves = Naves::find()->all();
        
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_camion]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelnaves' => $modelnaves,
        ]);
    }
    
    public function actionUpdate1($id)
    {
        $modelnaves = Naves::find()->all();
        
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo_camion]);
        }

        return $this->render('update_1', [
            'model' => $model,
            'modelnaves' => $modelnaves,
        ]);
    }

    /**
     * Deletes an existing Camiones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Camiones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Camiones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Camiones::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
