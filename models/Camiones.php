<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camiones".
 *
 * @property int $codigo_camion
 * @property string|null $matricula
 * @property string|null $modelo_completo
 * @property int|null $kilometros
 * @property string|null $motor
 * @property string|null $estado
 * @property float|null $consumo
 * @property string|null $tipo_de_chasis
 * @property int|null $codigo_nave
 * @property int|null $revision
 *
 * @property Cargas[] $cargas
 * @property Choferes[] $choferes
 * @property Naves $codigoNave
 */
class Camiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
   
    public $nombre_completo;
    
    
    
    public static function tableName()
    {
        return 'camiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kilometros', 'codigo_nave', 'revision'], 'integer'],
            [['kilometros'], 'required', 'message'=>'Debe introducir los kilómetros'],
            [['consumo'], 'number'],
            [['matricula'], 'string', 'max' => 7],
            [['matricula'], 'required', 'message'=>'Debe introducir una matrícula'],
            [['modelo_completo'], 'string', 'max' => 150],
            [['modelo_completo'], 'required', 'message'=>'Debe introducir el modelo'],
            [['motor', 'tipo_de_chasis'], 'string', 'max' => 100],
            [['estado'], 'string', 'max' => 50],
            [['estado'], 'required', 'message'=>'Debe seleccionar el estado'],
            [['codigo_nave'], 'exist', 'skipOnError' => true, 'targetClass' => Naves::className(), 'targetAttribute' => ['codigo_nave' => 'codigo_nave']],
            [['codigo_nave'], 'required', 'message'=>'Debe seleccionar una nave'],
            [['revision'], 'required', 'message'=>'Debe indicar si el camión necesita de mantenimiento'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_camion' => 'Código Camion',
            'matricula' => 'Matrícula',
            'modelo_completo' => 'Modelo Completo',
            'kilometros' => 'Kilómetros',
            'motor' => 'Motor',
            'estado' => 'Estado',
            'consumo' => 'Consumo',
            'tipo_de_chasis' => 'Tipo De Chasis',
            'codigo_nave' => 'Codigo Nave',
            'revision' => 'Revisión',
        ];
    }

    /**
     * Gets query for [[Cargas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCargas()
    {
        return $this->hasMany(Cargas::className(), ['codigo_camion' => 'codigo_camion']);
    }

    /**
     * Gets query for [[Choferes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChoferes()
    {
        return $this->hasMany(Choferes::className(), ['codigo_camion' => 'codigo_camion']);
    }

    /**
     * Gets query for [[CodigoNave]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoNave()
    {
        return $this->hasOne(Naves::className(), ['codigo_nave' => 'codigo_nave']);
    }
}
