<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fechas".
 *
 * @property int $codigo_fecha
 * @property string|null $fecha
 * @property int|null $codigo_paquete
 *
 * @property Paquetes $codigoPaquete
 */
class Fechas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fechas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['codigo_paquete'], 'integer'],
            [['fecha', 'codigo_paquete'], 'unique', 'targetAttribute' => ['fecha', 'codigo_paquete']],
            [['codigo_paquete'], 'exist', 'skipOnError' => true, 'targetClass' => Paquetes::className(), 'targetAttribute' => ['codigo_paquete' => 'codigo_paquete']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_fecha' => 'Codigo Fecha',
            'fecha' => 'Fecha',
            'codigo_paquete' => 'Codigo Paquete',
        ];
    }

    /**
     * Gets query for [[CodigoPaquete]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPaquete()
    {
        return $this->hasOne(Paquetes::className(), ['codigo_paquete' => 'codigo_paquete']);
    }
}
