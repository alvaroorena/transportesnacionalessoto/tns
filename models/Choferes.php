<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "choferes".
 *
 * @property int $codigo_chofer
 * @property string|null $nombre_completo
 * @property string|null $tipo_de_carga
 * @property string|null $estado
 * @property float|null $salario_kilometro
 * @property int|null $codigo_camion
 *
 * @property Camiones $codigoCamion
 * @property Rutas[] $rutas
 */
class Choferes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'choferes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['salario_kilometro'], 'number'],
            [['codigo_camion'], 'integer'],
            [['nombre_completo'], 'string', 'max' => 100],
            [['nombre_completo'], 'required', 'message'=>'Debe introducir un nombre para el conductor'],
            [['tipo_de_carga'], 'string', 'max' => 200],
            [['codigo_camion'], 'exist', 'skipOnError' => true, 'targetClass' => Camiones::className(), 'targetAttribute' => ['codigo_camion' => 'codigo_camion']],
            [['codigo_camion'], 'required', 'message'=>'Debe seleccionar un camión'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_chofer' => 'Código Chofer',
            'nombre_completo' => 'Nombre Completo',
            'tipo_de_carga' => 'Tipo De Carga',
            
            'salario_kilometro' => 'Salario Kilometro',
            'codigo_camion' => 'Código Camion',
        ];
    }

    /**
     * Gets query for [[CodigoCamion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCamion()
    {
        return $this->hasOne(Camiones::className(), ['codigo_camion' => 'codigo_camion']);
    }

    /**
     * Gets query for [[Rutas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRutas()
    {
        return $this->hasMany(Rutas::className(), ['codigo_chofer' => 'codigo_chofer']);
    }
}
