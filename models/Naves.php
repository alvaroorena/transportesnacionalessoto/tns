<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "naves".
 *
 * @property int $codigo_nave
 * @property string|null $nombre_responsable
 * @property string|null $ubicacion
 * @property string|null $telefono
 * @property int|null $metros_cuadrados
 * @property string|null $mapa
 *
 * @property Camiones[] $camiones
 * @property Furgonetas[] $furgonetas
 * @property Presupuestos[] $presupuestos
 */
class Naves extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'naves';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['metros_cuadrados'], 'integer'],
            [['nombre_responsable'], 'string', 'max' => 100],
            [['nombre_responsable'], 'required', 'message'=>'Debe introducir el nombre del responsable'],
            [['ubicacion'], 'string', 'max' => 150],
            [['ubicacion'], 'required', 'message'=>'Debe introducir la ubicación'],
            [['telefono'], 'string', 'max' => 12],
            [['telefono'], 'required', 'message'=>'Debe introducir un teléfono'],
            [['mapa'], 'string', 'max' => 4000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_nave' => 'Código Nave',
            'nombre_responsable' => 'Nombre Responsable',
            'ubicacion' => 'Ubicación',
            'telefono' => 'Teléfono',
            'metros_cuadrados' => 'Metros Cuadrados',
            'mapa' => 'Mapa',
        ];
    }

    /**
     * Gets query for [[Camiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamiones()
    {
        return $this->hasMany(Camiones::className(), ['codigo_nave' => 'codigo_nave']);
    }

    /**
     * Gets query for [[Furgonetas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFurgonetas()
    {
        return $this->hasMany(Furgonetas::className(), ['codigo_nave' => 'codigo_nave']);
    }

    /**
     * Gets query for [[Presupuestos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPresupuestos()
    {
        return $this->hasMany(Presupuestos::className(), ['codigo_nave' => 'codigo_nave']);
    }
}
