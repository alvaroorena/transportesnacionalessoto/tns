<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rutas".
 *
 * @property int $codigo_ruta
 * @property string|null $origen
 * @property string|null $destino
 * @property string|null $fecha
 * @property int|null $codigo_chofer
 *
 * @property Choferes $codigoChofer
 */
class Rutas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rutas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['codigo_chofer'], 'integer'],
            [['origen', 'destino'], 'string', 'max' => 100],
            [['codigo_chofer'], 'exist', 'skipOnError' => true, 'targetClass' => Choferes::className(), 'targetAttribute' => ['codigo_chofer' => 'codigo_chofer']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_ruta' => 'Codigo Ruta',
            'origen' => 'Origen',
            'destino' => 'Destino',
            'fecha' => 'Fecha',
            'codigo_chofer' => 'Codigo Chofer',
        ];
    }

    /**
     * Gets query for [[CodigoChofer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoChofer()
    {
        return $this->hasOne(Choferes::className(), ['codigo_chofer' => 'codigo_chofer']);
    }
}
