<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "repartidores".
 *
 * @property int $codigo_repartidor
 * @property string|null $nombre_completo
 * @property string|null $estado
 * @property float|null $salario_kilometro
 * @property int|null $codigo_furgoneta
 *
 * @property Furgonetas $codigoFurgoneta
 */
class Repartidores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'repartidores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['salario_kilometro'], 'number'],
            [['codigo_furgoneta'], 'integer'],
            [['nombre_completo'], 'string', 'max' => 100],
            [['nombre_completo'], 'required', 'message'=>'Debe introducir un nombre para el conductor'],
            [['codigo_furgoneta'], 'exist', 'skipOnError' => true, 'targetClass' => Furgonetas::className(), 'targetAttribute' => ['codigo_furgoneta' => 'codigo_furgoneta']],
            [['codigo_furgoneta'], 'required', 'message'=>'Debe seleccionar una furgoneta'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_repartidor' => 'Codigo Repartidor',
            'nombre_completo' => 'Nombre Completo',
            
            'salario_kilometro' => 'Salario Kilometro',
            'codigo_furgoneta' => 'Codigo Furgoneta',
        ];
    }

    /**
     * Gets query for [[CodigoFurgoneta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoFurgoneta()
    {
        return $this->hasOne(Furgonetas::className(), ['codigo_furgoneta' => 'codigo_furgoneta']);
    }
}
