<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "presupuestos".
 *
 * @property int $codigo_presupuesto
 * @property string|null $descripcion_carga_presupuestada
 * @property float|null $peso
 * @property float|null $valor
 * @property string|null $empresa_origen
 * @property string|null $empresa_destino
 * @property int|null $codigo_nave
 *
 * @property Naves $codigoNave
 */
class Presupuestos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'presupuestos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['peso', 'valor'], 'number'],
            [['codigo_nave'], 'integer'],
            [['descripcion_carga_presupuestada'], 'string', 'max' => 200],
            [['empresa_origen', 'empresa_destino'], 'string', 'max' => 150],
            [['codigo_nave'], 'exist', 'skipOnError' => true, 'targetClass' => Naves::className(), 'targetAttribute' => ['codigo_nave' => 'codigo_nave']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_presupuesto' => 'Código Presupuesto',
            'descripcion_carga_presupuestada' => 'Descripción Carga Presupuestada',
            'peso' => 'Peso',
            'valor' => 'Valor',
            'empresa_origen' => 'Empresa Origen',
            'empresa_destino' => 'Empresa Destino',
            'codigo_nave' => 'Código Nave',
        ];
    }

    /**
     * Gets query for [[CodigoNave]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoNave()
    {
        return $this->hasOne(Naves::className(), ['codigo_nave' => 'codigo_nave']);
    }
}
