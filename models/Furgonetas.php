<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "furgonetas".
 *
 * @property int $codigo_furgoneta
 * @property string|null $matricula
 * @property string|null $modelo_completo
 * @property int|null $kilometros
 * @property string|null $motor
 * @property string|null $estado
 * @property float|null $consumo
 * @property int|null $codigo_nave
 * @property int|null $revision
 *
 * @property Naves $codigoNave
 * @property Paquetes[] $paquetes
 * @property Repartidores[] $repartidores
 */
class Furgonetas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'furgonetas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kilometros', 'codigo_nave', 'revision'], 'integer'],
            [['kilometros'], 'required', 'message'=>'Debe introducir los kilómetros'],
            [['consumo'], 'number'],
            [['matricula'], 'string', 'max' => 7],
            [['matricula'], 'required', 'message'=>'Debe introducir una matrícula'],
            [['modelo_completo'], 'string', 'max' => 150],
            [['modelo_completo'], 'required', 'message'=>'Debe introducir el modelo'],
            [['motor'], 'string', 'max' => 100],
            [['estado'], 'string', 'max' => 50],
            [['codigo_nave'], 'exist', 'skipOnError' => true, 'targetClass' => Naves::className(), 'targetAttribute' => ['codigo_nave' => 'codigo_nave']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_furgoneta' => 'Código Furgoneta',
            'matricula' => 'Matrícula',
            'modelo_completo' => 'Modelo Completo',
            'kilometros' => 'Kilómetros',
            'motor' => 'Motor',
            'estado' => 'Estado',
            'consumo' => 'Consumo',
            'codigo_nave' => 'Codigo Nave',
            'revision' => 'Revisión',
        ];
    }

    /**
     * Gets query for [[CodigoNave]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoNave()
    {
        return $this->hasOne(Naves::className(), ['codigo_nave' => 'codigo_nave']);
    }

    /**
     * Gets query for [[Paquetes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetes()
    {
        return $this->hasMany(Paquetes::className(), ['codigo_furgoneta' => 'codigo_furgoneta']);
    }

    /**
     * Gets query for [[Repartidores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRepartidores()
    {
        return $this->hasMany(Repartidores::className(), ['codigo_furgoneta' => 'codigo_furgoneta']);
    }
}
