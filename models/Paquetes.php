<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paquetes".
 *
 * @property int $codigo_paquete
 * @property string|null $direccion_entrega
 * @property string|null $indicaciones_conductor
 * @property string|null $nombre_destinatario
 * @property string|null $telefono_destinatario
 * @property float|null $peso
 * @property string|null $fecha_entrega
 * @property int|null $codigo_furgoneta
 *
 * @property Furgonetas $codigoFurgoneta
 * @property Fechas[] $fechas
 */
class Paquetes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paquetes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['peso'], 'number'],
            [['fecha_entrega'], 'safe'],
            [['codigo_furgoneta'], 'integer'],
            [['direccion_entrega', 'indicaciones_conductor'], 'string', 'max' => 200],
            [['direccion_entrega'], 'required', 'message'=>'Debe introducir una dirección'],
            [['fecha_entrega'], 'required', 'message'=>'Debe introducir una fecha de entrega'],
            [['nombre_destinatario'], 'string', 'max' => 100],
            [['nombre_destinatario'], 'required', 'message'=>'Debe introducir un destinatario'],
            [['telefono_destinatario'], 'string', 'max' => 12],
            [['telefono_destinatario'], 'required', 'message'=>'Debe introducir un teléfono'],
            [['codigo_furgoneta'], 'exist', 'skipOnError' => true, 'targetClass' => Furgonetas::className(), 'targetAttribute' => ['codigo_furgoneta' => 'codigo_furgoneta']],
            [['codigo_furgoneta'], 'required', 'message'=>'Debe seleccionar una furgoneta'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_paquete' => 'Código Paquete',
            'direccion_entrega' => 'Dirección Entrega',
            'indicaciones_conductor' => 'Indicaciones Conductor',
            'nombre_destinatario' => 'Nombre Destinatario',
            'telefono_destinatario' => 'Teléfono Destinatario',
            'peso' => 'Peso',
            'fecha_entrega' => 'Fecha Entrega',
            'codigo_furgoneta' => 'Código Furgoneta',
        ];
    }

    /**
     * Gets query for [[CodigoFurgoneta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoFurgoneta()
    {
        return $this->hasOne(Furgonetas::className(), ['codigo_furgoneta' => 'codigo_furgoneta']);
    }

    /**
     * Gets query for [[Fechas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFechas()
    {
        return $this->hasMany(Fechas::className(), ['codigo_paquete' => 'codigo_paquete']);
    }

    public function afterFind() {
        parent::afterFind();
        $this->fecha_entrega=Yii::$app->formatter->asDate($this->fecha_entrega, 'php:d/m/Y');
    }
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        $this->fecha_entrega= \DateTime::createFromFormat("d/m/Y", $this->fecha_entrega)->format("Y/m/d");
        return true;
    }
    
    }


