<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cargas".
 *
 * @property int $codigo_carga
 * @property string|null $descripcion_carga
 * @property string|null $indicaciones_conductor
 * @property float|null $peso
 * @property string|null $empresa_origen
 * @property string|null $empresa_destino
 * @property string|null $fecha_entrega
 * @property int|null $codigo_camion
 *
 * @property Camiones $codigoCamion
 */
class Cargas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cargas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['peso'], 'number'],
            [['fecha_entrega'], 'safe'],
            [['fecha_entrega'], 'required', 'message'=>'Debe introducir una fecha de partida'],
            [['codigo_camion'], 'integer'],
            [['descripcion_carga', 'indicaciones_conductor'], 'string', 'max' => 200],
            [['descripcion_carga'], 'required', 'message'=>'Debe introducir una descripción para la carga'],
            [['empresa_origen', 'empresa_destino'], 'string', 'max' => 150],
            [['empresa_origen'], 'required', 'message'=>'Debe seleccionar la empresa de origen'],
            [['empresa_destino'], 'required', 'message'=>'Debe seleccionar la empresa de destino'],
            [['codigo_camion'], 'exist', 'skipOnError' => true, 'targetClass' => Camiones::className(), 'targetAttribute' => ['codigo_camion' => 'codigo_camion']],
            [['codigo_camion'], 'required', 'message'=>'Debe seleccionar un camión'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo_carga' => 'Código Carga',
            'descripcion_carga' => 'Descripción Carga',
            'indicaciones_conductor' => 'Indicaciones Conductor',
            'peso' => 'Peso',
            'empresa_origen' => 'Empresa Origen',
            'empresa_destino' => 'Empresa Destino',
            'fecha_entrega' => 'Fecha Partida',
            'codigo_camion' => 'Código Camion',
        ];
    }

    /**
     * Gets query for [[CodigoCamion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCamion()
    {
        return $this->hasOne(Camiones::className(), ['codigo_camion' => 'codigo_camion']);
    }
    
   public function afterFind() {
        parent::afterFind();
        $this->fecha_entrega=Yii::$app->formatter->asDate($this->fecha_entrega, 'php:d/m/Y');
    }
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        $this->fecha_entrega= \DateTime::createFromFormat("d/m/Y", $this->fecha_entrega)->format("Y/m/d");
        return true;
    }
}
