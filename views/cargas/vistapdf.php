<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cargas */

$this->title = "CARGA CON REFERENCIA : ".$model->codigo_carga;
$this->params['breadcrumbs'][] = ['label' => 'Cargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cargas-view">

    <h1><?= Html::encode($this->title) ?></h1>

   

    <?= Html::img('../web/img/pdf.png');?>
    
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_carga',
            'descripcion_carga',
            'indicaciones_conductor',
            'peso',
            'fecha_entrega',
            'empresa_origen',
            'empresa_destino',
            
            
            [
                    'attribute' => 'matricula',
                    'label' => 'Matrícula',
                    'value' => $model->codigoCamion['matricula']
                      
                    
                ],
            
        ],
    ]) ?>
    
    

</div>