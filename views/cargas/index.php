<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'CARGAS Y ALBARANES';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cargas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar Carga', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

//            'codigo_carga',
            'descripcion_carga',
            'indicaciones_conductor',
            'peso',
            'fecha_entrega',
            'empresa_origen',
            'empresa_destino',
            //'codigo_camion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
