<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cargas */

$this->title = "CARGA CON REFERENCIA : ".$model->codigo_carga;
$this->params['breadcrumbs'][] = ['label' => 'Cargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cargas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_carga], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_carga], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de querer eliminar la carga?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    
    
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_carga',
            'descripcion_carga',
            'indicaciones_conductor',
            'peso',
            'fecha_entrega',
            'empresa_origen',
            'empresa_destino',
            
            
            [
                    'attribute' => 'matricula',
                    'label' => 'Matrícula',
                    'value' => $model->codigoCamion['matricula']
                      
                    
                ],
            
        ],
    ]) ?>
    
    <?= Html::a('Generar Albarán', ['viewpdf', 'id' => $model->codigo_carga], ['class' => 'btn btn-primary']) ?>

</div>

