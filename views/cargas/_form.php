<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\icons\FontAwesomeAsset;
FontAwesomeAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Cargas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cargas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'descripcion_carga')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'indicaciones_conductor')->textInput(['maxlength' => true]) ?>

    

    <?= $form->field($model, 'peso')->textInput(['placeholder' => 'Ejemplo: 9500', 'maxlength' => true]) ?>
    <?php
    echo $form->field($model, 'fecha_entrega')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Introduce una fecha de partida'],
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'dd/mm/yyyy',
         'todayHighlight' => true,
                'todayBtn' => true,
    ]
]);
    
    ?>

    

    
    <?php
    
    $tipos = 
            [
             'TNS Barcelona' => 'TNS Barcelona',
             'TNS Madrid' => 'TNS Madrid',
             'TNS Santander' => ' TNS Santander',
             'TNS Sevilla' => 'TNS Sevilla',
             'TNS Valencia' => 'TNS Valencia',
             'TNS Vigo' => 'TNS Vigo',
             'TNS Zaragoza' => 'TNS Zaragoza',
             
             
                
            ];
    
            echo $form->field($model, 'empresa_origen')
               ->dropDownList($tipos,['prompt' => 'Seleccione punto de origen'])
               -> label ('Empresa Origen');
    ?>

    <?php
    
    $tipos = 
            [
             'TNS Barcelona' => 'TNS Barcelona',
             'TNS Madrid' => 'TNS Madrid',
             'TNS Santander' => ' TNS Santander',
             'TNS Sevilla' => 'TNS Sevilla',
             'TNS Valencia' => 'TNS Valencia',
             'TNS Vigo' => 'TNS Vigo',
             'TNS Zaragoza' => 'TNS Zaragoza',
             
             
                
            ];
    
            echo $form->field($model, 'empresa_destino')
               ->dropDownList($tipos,['prompt' => 'Seleccione punto de destino'])
               -> label ('Empresa Destino');
    ?>

 
        
     <?php
       $mapcamiones = ArrayHelper::map($modelcamiones, 'codigo_camion','matricula','ubicacion','modelo_completo');
               echo $form->field($model, 'codigo_camion')
               ->dropDownList($mapcamiones,['prompt' => 'Seleccione un Camión'])
               -> label ('Camión');
        
        ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
