<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cargas */

$this->title = 'ACTUALIZAR CARGA: ' . $model->codigo_carga;
$this->params['breadcrumbs'][] = ['label' => 'Cargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_carga, 'url' => ['view', 'id' => $model->codigo_carga]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cargas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelcamiones' => $modelcamiones,
    ]) ?>

</div>
