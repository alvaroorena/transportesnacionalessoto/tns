<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Camiones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="camiones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'matricula')-> label ('Matrícula')->textInput(['placeholder' => 'Ejemplo: 5805GTZ', 'maxlength' => true]) ?>

    <?php
    
    $tipos = 
            [
             'Daf XF 105' => 'Daf XF 105',
             'Daf XG' => 'Daf XG',
             'Iveco Stralis Active Space' => 'Iveco Stralis Active Space',
             'Man TGX' => 'Man TGX',
             'Mercedes Benz Actros MegaSpace' => 'Mercedes Benz Actros MegaSpace',
             'Mercedes Benz New Actros' => 'Mercedes Benz New Actros',
             'Renault Magnum Excellence' => 'Renault Magnum Excellence',
             'Renault T High Sleeper' => 'Renault T High Sleeper',
             'Scania Streamline' => 'Scania Streamline',
             'Scania R' => 'Scania R',
             'Volvo FH16' => 'Volvo FH16',
             'Volvo FH Globetrotter XL' => 'Volvo FH Globetrotter XL',
             
                
             
                
            ];
    
            echo $form->field($model, 'modelo_completo')
               ->dropDownList($tipos,['prompt' => 'Seleccione una cabina'])
               -> label ('Seleccionar Modelo');
    ?>

    <?= $form->field($model, 'kilometros')-> label ('Kilómetros')->textInput(['placeholder' => 'Ejemplo : 145765']) ?>

    <?= $form->field($model, 'motor')->textInput(['placeholder' => ' Ejemplo: 423cv / 2620Nm']) ?>
    

    <?php
    
    $tipos = 
            [
             'Disponible' => 'Disponible',
             'Trabajando' => 'Trabajando',
                
             
                
            ];
    
            echo $form->field($model, 'estado')
               ->dropDownList($tipos,['prompt' => 'Seleccione un estado'])
               -> label ('Seleccionar Estado');
    ?>

    <?= $form->field($model, 'consumo')->textInput (['placeholder' => 'Ejemplo : 24.4']) ?>

    <?php
    
    $tipos = 
            [
             '4x2' => '4x2',
             '6x2' => '6x2',
             '6x4' => '6x4',
             '4x2 con trasera elevable' => '4x2 con trasera elevable',
             '6x2 con trasera elevable' => '6x2 con trasera elevable',
             '6x4 con trasera elevable' => '6x4 con trasera elevable',
             
             
                
            ];
    
            echo $form->field($model, 'tipo_de_chasis')
               ->dropDownList($tipos,['prompt' => 'Seleccione un tipo de chasis'])
               -> label ('Seleccionar Chasis');
    ?>

    
    
        <?php
       $mapnaves = ArrayHelper::map($modelnaves, 'codigo_nave', 'ubicacion');
               echo $form->field($model, 'codigo_nave')
               ->dropDownList($mapnaves,['prompt' => 'Seleccione una Nave'])
               -> label ('Nave');
        
        ?>

    <?php
    
    $revision = 
            [
             '1' => 'Mantenimiento Realizado',
             '0' => 'Mantenimiento no Realizado',
             
             
             
                
            ];
    
            echo $form->field($model, 'revision')
               ->dropDownList($revision,['prompt' => 'Seleccione si el camión necesita de algún tipo de revisión'])
               -> label ('Mantenimiento');
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
