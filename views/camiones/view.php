<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Camiones */

$this->title = $model->modelo_completo." ".$model->matricula;
$prueba = $model->modelo_completo.$model->matricula;
$this->params['breadcrumbs'][] = ['label' => 'Camiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $prueba;
\yii\web\YiiAsset::register($this);
?>

<div class="camiones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_camion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_camion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de querer eliminar el camión?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'codigo_camion',
            //'matricula',
            //'modelo_completo',
            [
                    'attribute' => 'kilometros',
                    'label' => 'Kilómetros',
                    'value' => $model->kilometros.' kilómetros'
                      
                    
                ],
//            'kilometros',
            'motor',
            'estado',
            [
                    'attribute' => 'consumo',
                    'label' => 'Consumo',
                    'value' => $model->consumo.' litros'
                      
                    
                ],
            
            'tipo_de_chasis',
            [
                    'attribute' => 'ubicacion',
                    'label' => 'Nave de Origen',
                    'value' => $model->codigoNave['ubicacion']
                      
                    
                ],

            [
             'attribute' => 'revision',
             'format'=>'raw',
             'value'=> function ($model) {
                        if($model->revision == 1)
                        {

                            return 'El camión no necesita ningún mantenimiento';

                        } 
                        else {
                        return 'El camión necesita de mantenimiento';
                        }
                      },
        ],
        ],
    ]) ?>
    
    

</div>
