<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'CAMIONES';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="camiones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar Camión', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'codigo_camion',
            'matricula',
            'modelo_completo',
            'kilometros',
            'motor',
            //'estado',
            //'consumo',
            //'tipo_de_chasis',
            //'codigo_nave',
            //'revision',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
