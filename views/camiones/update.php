<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Camiones */

$this->title = 'ACTUALIZAR CAMIÓN CON MATRÍCULA: ' . $model->matricula;
$this->params['breadcrumbs'][] = ['label' => 'Camiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_camion, 'url' => ['view', 'id' => $model->codigo_camion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="camiones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelnaves' => $modelnaves,
    ]) ?>

</div>
