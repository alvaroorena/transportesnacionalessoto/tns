<?php

use yii\helpers\Html;

use yii\helpers\ArrayHelper;
use yii\bootstrap4\Modal;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\icons\FontAwesomeAsset;
FontAwesomeAsset::register($this);


/* @var $this yii\web\View */
/* @var $model app\models\Paquetes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paquetes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'direccion_entrega')->textInput(['maxlength' => true]) ?>

     <?= $form->field($model, 'nombre_destinatario')->textInput(['maxlength' => true]) ?>
           
     <?= $form->field($model, 'indicaciones_conductor')->textInput(['maxlength' => true]) ?>

     <?php
    echo $form->field($model, 'fecha_entrega')->widget(DatePicker::classname(), [
    'options' => ['placeholder' => 'Introduce fecha de entrega'],
    'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'dd/mm/yyyy',
         'todayHighlight' => true,
                'todayBtn' => true,
    ]
]);
    
    ?>

    <?= $form->field($model, 'telefono_destinatario')->textInput(['placeholder' => 'Ejemplo : 687645232']) ?>

    <?= $form->field($model, 'peso')->textInput(['placeholder' => 'Ejemplo: 2.8', 'maxlength' => true]) ?>

    <?php
       $mapfurgonetas = ArrayHelper::map($modelfurgonetas, 'codigo_furgoneta', 'matricula','modelo_completo');
               echo $form->field($model, 'codigo_furgoneta')
               ->dropDownList($mapfurgonetas,['prompt' => 'Seleccione una Furgoneta'])
               -> label ('Furgoneta');
        
        ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
