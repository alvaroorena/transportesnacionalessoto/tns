<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Paquetes */

$this->title = "PAQUETE CON REFERENCIA : ".$model->codigo_paquete;
$this->params['breadcrumbs'][] = ['label' => 'Paquetes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="paquetes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_paquete], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_paquete], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de querer eliminar el paquete?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

     <?php
//    $nombre = app\models\Furgonetas::find ()
//       ->select ('matricula')
//       ->where ('codigo_furgoneta='.$model->codigo_furgoneta)
//          ->scalar();
   ?>

    
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_paquete',
            'direccion_entrega',
            'indicaciones_conductor',
            'nombre_destinatario',
            'telefono_destinatario',
            'peso',
            'fecha_entrega',
            
            [
                    'attribute' => 'matricula',
                    'label' => 'Matrícula',
                    'value' => $model->codigoFurgoneta['matricula']
                      
                    
                ],
            
        ],
    ]) ?>

</div>
