<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'PAQUETES';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paquetes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar Paquete', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

//            'codigo_paquete',
            'direccion_entrega',
            'indicaciones_conductor',
            'nombre_destinatario',
            'telefono_destinatario',
            //'peso',
            'fecha_entrega',
            //'codigo_furgoneta',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
