<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Repartidores */

$this->title = 'REGISTRO DE REPARTIDOR';
$this->params['breadcrumbs'][] = ['label' => 'Repartidores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repartidores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelfurgonetas' => $modelfurgonetas,
    ]) ?>

</div>
