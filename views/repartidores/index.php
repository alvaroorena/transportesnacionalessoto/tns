<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'REPARTIDORES';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repartidores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar Repartidor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
//        'model' => $model,
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'codigo_repartidor',
            'nombre_completo',
            
            'salario_kilometro',
//            [
//                    'attribute' => 'matricula',
//                    'label' => 'Matrícula',
//                    'value' => $model->codigoFurgoneta['matricula']
//                      
//                    
//                ],
//            'codigo_furgoneta',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
