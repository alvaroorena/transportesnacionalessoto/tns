<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Repartidores */

$this->title = 'Actualizar Repartidor: ' . $model->nombre_completo;
$this->params['breadcrumbs'][] = ['label' => 'Repartidores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_repartidor, 'url' => ['view', 'id' => $model->codigo_repartidor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="repartidores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelfurgonetas' => $modelfurgonetas,
    ]) ?>

</div>
