<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Repartidores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="repartidores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_completo')->textInput(['maxlength' => true]) ?>

     

   <?= $form->field($model, 'salario_kilometro')->textInput(['placeholder' => 'Ejemplo : 0.14']) ?>

   <?php
       $mapfurgonetas = ArrayHelper::map($modelfurgonetas, 'codigo_furgoneta', 'matricula', 'modelo_completo');
               echo $form->field($model, 'codigo_furgoneta')
               ->dropDownList($mapfurgonetas,['prompt' => 'Seleccione un Camión'])
               -> label ('Furgoneta');
        
        ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
