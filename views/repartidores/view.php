<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Repartidores */

$this->title = $model->nombre_completo;
$this->params['breadcrumbs'][] = ['label' => 'Repartidores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="repartidores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_repartidor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_repartidor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de querer eliminar el repartidor?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'codigo_repartidor',
            'nombre_completo',
            
            'salario_kilometro',
            
            
             
            [
                    'attribute' => 'matricula',
                    'label' => 'Matrícula',
                    'value' => $model->codigoFurgoneta['matricula']
                      
                    
                ],
        ],
    ]) ?>

</div>
