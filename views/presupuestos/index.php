<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Presupuestos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="presupuestos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar Presupuesto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'codigo_presupuesto',
            'descripcion_carga_presupuestada',
            'peso',
            'valor',
            'empresa_origen',
            //'empresa_destino',
            //'codigo_nave',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
