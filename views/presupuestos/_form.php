<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Presupuestos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="presupuestos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'descripcion_carga_presupuestada')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'peso')->textInput() ?>

    <?= $form->field($model, 'valor')->textInput() ?>

    <?= $form->field($model, 'empresa_origen')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'empresa_destino')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_nave')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
