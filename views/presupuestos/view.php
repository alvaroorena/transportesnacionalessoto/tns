<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Presupuestos */

$this->title = $model->codigo_presupuesto;
$this->params['breadcrumbs'][] = ['label' => 'Presupuestos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="presupuestos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_presupuesto], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_presupuesto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo_presupuesto',
            'descripcion_carga_presupuestada',
            'peso',
            'valor',
            'empresa_origen',
            'empresa_destino',
            'codigo_nave',
            
             
            [
                    'attribute' => 'codigo_nave',
                    'label' => 'Ubicación Presupuesto',
                    'value' => $model->codigoNave['ubicacion']
                      
                    
                ],
        ],
    ]) ?>

</div>
