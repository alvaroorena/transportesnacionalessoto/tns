<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'NAVES';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="naves-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar Nave', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            //'codigo_nave',
            'nombre_responsable',
            'ubicacion',
            'telefono',
            'metros_cuadrados',
            //'mapa',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
