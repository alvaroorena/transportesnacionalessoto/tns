<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Naves */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="naves-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_responsable')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ubicacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono')->textInput(['placeholder' => 'Ejemplo : 942576458']) ?>

    <?= $form->field($model, 'metros_cuadrados')->textInput(['placeholder' => 'Ejemplo : 3500']) ?>
    
    <?= $form->field($model, 'mapa')->textInput(['placeholder' => 'Insertar Mapa de Google']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

