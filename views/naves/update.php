<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Naves */

$this->title = 'ACTUALIZAR NAVE: ' . $model->ubicacion;
$this->params['breadcrumbs'][] = ['label' => 'Naves', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_nave, 'url' => ['view', 'codigo_nave' => $model->codigo_nave]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="naves-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
