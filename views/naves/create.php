<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Naves */

$this->title = 'REGISTRO DE NAVE';
$this->params['breadcrumbs'][] = ['label' => 'Naves', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="naves-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
