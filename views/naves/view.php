<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Naves */

$this->title = $model->ubicacion;
$this->params['breadcrumbs'][] = ['label' => 'Naves', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="naves-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'codigo_nave' => $model->codigo_nave], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'codigo_nave' => $model->codigo_nave], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de querer eliminar la nave?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'codigo_nave',
            'nombre_responsable',
            'ubicacion',
            'telefono',
            'metros_cuadrados',
           // 'mapa',
        ],
    ]) ?>

</div>
