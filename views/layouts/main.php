<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
    
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
//        'brandLabel' => Yii::$app->name,
        'brandLabel' => '<img src="'.Yii::$app->getUrlManager()->getBaseUrl().'/img/logo.png">',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md my-navbar  fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
                ['label' => 'CARGAS', 'url' => ['/cargas/index']],
                ['label' => 'PAQUETES', 'url' => ['/paquetes/index']],
                ['label' => 'LOCALIZACIONES', 'url' => ['/site/mostrarnaves']],
                
//            ['label' => 'About', 'url' => ['/site/about']],
//            ['label' => 'Contact', 'url' => ['/site/contact']],
//            ['label' => 'About', 'url' => ['/site/about']],
//            ['label' => 'Contact', 'url' => ['/site/contact']],
//            ['label' => 'About', 'url' => ['/site/about']],
//            ['label' => 'Contact', 'url' => ['/site/contact']],
//            ['label' => 'Home', 'url' => ['/site/index']],
//            ['label' => 'About', 'url' => ['/site/about']],
//            ['label' => 'Contact', 'url' => ['/site/contact']],
//            ['label' => 'Camiones', 'url' => ['/camiones/index']],
//            ['label' => 'Furgonetas', 'url' => ['/furgonetas/index']],
//            ['label' => 'Chóferes', 'url' => ['/choferes/index']],
//            ['label' => 'Repartidores', 'url' => ['/repartidores/index']],
//            ['label' => 'Cargas', 'url' => ['/cargas/index']],
//            ['label' => 'Paquetes', 'url' => ['/paquetes/index']],
//            ['label' => 'Naves', 'url' => ['/naves/index']],
//            ['label' => 'Rutas', 'url' => ['/rutas/index']],
//            ['label' => 'Fechas', 'url' => ['/fechas/index']],
//            ['label' => 'Fechas', 'url' => ['/presupuestos/index']],

//            Yii::$app->user->isGuest ? (
//                ['label' => 'LOGIN', 'url' => ['/site/login']]
//            ) : (
//                '<li>'
//                . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
//                . Html::submitButton(
//                    'Logout (' . Yii::$app->user->identity->username . ')',
//                    ['class' => 'btn btn-link logout']
//                )
//                . Html::endForm()
//                . '</li>'
//            )
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; Transportes Nacionales Soto <?= date('Y') ?></p>
        <p class="float-right"><?= 'Desarrollado por Álvaro Oreña Gómez' ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
