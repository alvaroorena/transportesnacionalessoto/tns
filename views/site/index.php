<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'TNS® HOME';
?>
<div class="site-index">


    
    <div class="body-content  ">

        <div class="rowhome test" >
         <a href="camiones/index" > 
            <div class="col-sm-3 ">
                <?= Html::img('../web/img/trucksbutton.png');?>
            </div>
        </a>
            <a href="choferes/index" > 
            <div class="col-sm-3 ">
                <?= Html::img('../web/img/choferes.png');?>
            </div>
                </a>
            <a href="camiones/consultareparaciones" > 
                
                
            <div class="col-sm-3 ">
               <?= Html::img('../web/img/repairsbutton.png');?> 
            </div>
                </a>
            <a href="site/vistabusquedas" > 
            <div class="col-sm-3 ">
                <?= Html::img('../web/img/searchbutton.png');?>
            </div>
                </a>
        </div>
        
        
         <div class="rowhome test">
            <a href="furgonetas/index" > 
             <div class="col-sm-3 ">
                <?= Html::img('../web/img/vansbutton.png');?>
            </div>
                </a>
             <a href="repartidores/index" > 
            <div class="col-sm-3 ">
                <?= Html::img('../web/img/repartidores.png');?>
            </div>
                 </a>
                 <a href="cargas/index" > 
            <div class="col-sm-3">
                <?= Html::img('../web/img/deliverynotesbutton.png');?>
            </div>
                     </a>
            <a href="paquetes/index" > 
            <div class="col-sm-3 ">
                <?= Html::img('../web/img/paquetes.png');?>
            </div>
                </a>
        </div>

    </div>
</div>
