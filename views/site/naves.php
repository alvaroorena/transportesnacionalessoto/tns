<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
$titulo = "LOCALIZACIONES";

?>


<div class ="well well-sm"> <h2 style="text-align: center; max-height: 50px; font-family: fugaz; color: #262650; font-size: 2.5rem;" ><?= $titulo ?> </h2> </div>



<p style="text-align: center; font-family: fugaz">
    
   <?= Html::a('Acceso Naves', ['naves/index'], ['class' => 'btn btn-success']) ?>
    
</p>

<div>
    <?= Listview::widget([
        'id'=>'naves',
    'dataProvider' => $dataProvider,
    'itemView' => '_naves',
    'layout' => "\n{items}",
        
    ]);
?>
    
</div>

