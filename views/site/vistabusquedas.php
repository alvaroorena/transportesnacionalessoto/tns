<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'TNS® BÚSQUEDAS';
?>

<div class ="well well-sm"> <h2 style=" max-height: 50px; font-family: fugaz; color: #262650; font-size: 2.5rem; margin-bottom: 45px; margin-left: 463px;" ><?= "BÚSQUEDAS" ?> </h2> </div>
<div class="site-index">


    
    
    <div class="body-content botonesconsultas  ">

        <div class="rowhome margenesconsultas" >
          
            
            <div class="col-sm-3  ">
                <?= Html::a( Html::img('@web/img/camionesdisponibles.png', ['alt' => 'Consulta1']),['camiones/consultadef']) ?>
                
            </div>
                
        
             
            <div class="col-sm-3 ">
                <?= Html::a( Html::img('@web/img/camionestrabajando.png', ['alt' => 'Consulta2']),['camiones/consultadef2']) ?>
            </div>
                
           
            
                
            
            
                
        </div>
        
        
         <div class="rowhome margenesconsultas">
            
              <div class="col-sm-3 ">
                <?= Html::a( Html::img('@web/img/furgonetasdisponibles.png', ['alt' => 'Consulta7']),['furgonetas/consultadisponibilidadfurgonetas']) ?>
            </div>
        
             
            <div class="col-sm-3 ">
                <?= Html::a( Html::img('@web/img/furgonetastrabajando.png', ['alt' => 'Consulta8']),['furgonetas/consultadisponfur']) ?>
            </div>
                 
                 
            
                     
            
                
        </div>
        
        
         <div class="rowhome   margenesconsultas" >
          
           <div class="col-sm-3 ">
                 <?= Html::a( Html::img('@web/img/kilometrajecamiones.png', ['alt' => 'Consulta3']),['camiones/consultakilometrajecamiones']) ?>
            </div>
                
             <div class="col-sm-3">
                <?= Html::a( Html::img('@web/img/consumocamiones.png', ['alt' => 'Consulta6']),['camiones/consultaconsumocamiones']) ?>
            </div>
            
            
             
           
                
        </div>
        
        
        <div class="rowhome filaconsultas2">
            
            <div class="col-sm-3 ">
               <?= Html::a( Html::img('@web/img/kilometrajefurgonetas.png', ['alt' => 'Consulta9']),['furgonetas/consultakilometrajefurgonetas']) ?>
            </div>
                 
                 
            <div class="col-sm-3">
               <?= Html::a( Html::img('@web/img/consumofurgonetas.png', ['alt' => 'Consulta12']),['furgonetas/consultaconsumofurgonetas']) ?>
            </div>
                     
            
                
        </div>
        
        

    </div>
</div>
