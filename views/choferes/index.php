<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'CHÓFERES';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="choferes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar Chófer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

//            'codigo_chofer',
            'nombre_completo',
            'tipo_de_carga',
            
            'salario_kilometro',
            //'codigo_camion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
