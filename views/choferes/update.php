<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Choferes */

$this->title = 'ACTUALIZAR CHÓFER: ' . $model->nombre_completo;
$this->params['breadcrumbs'][] = ['label' => 'Choferes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_chofer, 'url' => ['view', 'id' => $model->codigo_chofer]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="choferes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelcamiones' => $modelcamiones,
    ]) ?>

</div>
