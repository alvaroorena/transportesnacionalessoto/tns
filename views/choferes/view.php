<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Choferes */

$this->title = $model->nombre_completo;
$this->params['breadcrumbs'][] = ['label' => 'Choferes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="choferes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_chofer], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_chofer], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de querer eliminar el chófer?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    
        
        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'codigo_chofer',
            'nombre_completo',
            'tipo_de_carga',
            
            'salario_kilometro',
           // 'codigo_camion',
            
             [
                    'attribute' => 'matricula',
                    'label' => 'Matrícula',
                    'value' => $model->codigoCamion['matricula']
                      
                    
                ],
            
            
        ],
    ]) ?>

</div>
