<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Choferes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="choferes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_completo')->textInput(['maxlength' => true]) ?>

    
    <?php
    
    $tipos = 
            [
             'Alimentos' => 'Alimentos',
             'Cargas Frágiles' => 'Cargas Frágiles',
             'Gasóleo' => 'Gasóleo',
             'Material Industrial' => 'Material Industrial',
             'Material Sanitario' => 'Material Sanitario',
             'Mercancía Peligrosa' => 'Mercancía Peligrosa',
             'Otros' => 'Otros',
             
             
                
            ];
    
            echo $form->field($model, 'tipo_de_carga')
               ->dropDownList($tipos,['prompt' => 'Seleccione Carga'])
               -> label ('Carga Cualificada');
    ?>

    

    <?= $form->field($model, 'salario_kilometro')->textInput(['placeholder' => 'Ejemplo : 0.23']) ?>

    <?php
       $mapcamiones = ArrayHelper::map($modelcamiones, 'codigo_camion', 'matricula', 'modelo_completo');
               echo $form->field($model, 'codigo_camion')
               ->dropDownList($mapcamiones,['prompt' => 'Seleccione un Camión'])
               -> label ('Camión');
        
        ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
