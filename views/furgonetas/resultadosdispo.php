<?php

use yii\grid\GridView;
use yii\helpers\Html;
?>

<div class="jumbotron">
    <h2><?=$titulo?></h2>
    
    <p class="lead"><?= $enunciado ?> </p>
    <div class="well">
        <?= Html::a('Búsquedas', ['site/vistabusquedas'], ['class' => 'btn btn-success']) ?>
    <?= $sql ?>
    </div>
    
</div>

<?=
   GridView::widget([
   'dataProvider' => $resultadosdispo,
   'columns' => $campos,'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'codigo_camion',
            'matricula',
            'modelo_completo',
           // 'ubicacion',
//       [
//                    'attribute' => 'ubicacion',
//                    'label' => 'Nave de Origen',
//                    'value' => $model->codigoNave['ubicacion']
//                      
//                    
//                ],
            //'kilometros',
            //'motor',
            //'estado',
            //'consumo',
            //'tipo_de_chasis',
            //'codigo_nave',
            //'revision',

           // ['class' => 'yii\grid\ActionColumn'],
        ],
           
           
]); ?>