<?php

use yii\grid\GridView;
use yii\helpers\Html;
?>

<div class="jumbotron">
    <h2><?=$titulo?></h2>
    
    <p class="lead"><?= $enunciado ?> </p>
    <div class="well">
    <?= Html::a('Camiones', ['camiones/consultareparaciones'], ['class' => 'btn btn-success']) ?>
    <?= $sql ?>
    </div>
    
</div>

<?=
   GridView::widget([
       
   'dataProvider' => $resultadosrepara,
   'columns' => $campos,'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'codigo_camion',
            'matricula',
            'modelo_completo',
           // 'ubicacion',
//       [
//                    'attribute' => 'ubicacion',
//                    'label' => 'Nave de Origen',
//                    'value' => $model->codigoNave['ubicacion']
//                      
//                    
//                ],
            //'kilometros',
            //'motor',
            //'estado',
            //'consumo',
            //'tipo_de_chasis',
            //'codigo_nave',
            //'revision',

            [

        'class' => 'yii\grid\ActionColumn',

        'template' => '{update1} ',

        'buttons' => [
            'update1' => function ($url, $model) {
                return Html::a('REPARAR', $url);
            },

          
          'urlCreator' => function ($action, $model, $key, $index) {
            

            if ($action === 'update1') {
                $url ='index.php/furgonetas/update1?id='.$model->codigo_furgoneta;
                return $url;
            }
            

          }
    ],

    ],
        ],
   
           
           
]); ?>