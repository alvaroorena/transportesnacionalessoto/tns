<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Furgonetas */

$this->title = 'ESTADO DE MANTENIMIENTO DE LA FURGONETA CON MATRÍCULA: ' . $model->matricula;
$this->params['breadcrumbs'][] = ['label' => 'Furgonetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo_furgoneta, 'url' => ['view', 'id' => $model->codigo_furgoneta]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="furgonetas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_1', [
        'model' => $model,
        'modelnaves' => $modelnaves,
    ]) ?>

</div>
