<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Furgonetas */

$this->title = $model->modelo_completo." ".$model->matricula;
$this->params['breadcrumbs'][] = ['label' => 'Furgonetas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="furgonetas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->codigo_furgoneta], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->codigo_furgoneta], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de querer eliminar la furgoneta?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    
    
        
        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'codigo_furgoneta',
            //'matricula',
            //'modelo_completo',
            'kilometros',
            'motor',
            'estado',
            'consumo',
//            'revision',
//            'codigo_nave',
            [
             'attribute' => 'revision',
             'format'=>'raw',
             'value'=> function ($model) {
                        if($model->revision == 1)
                        {

                            return 'La furgoneta no necesita ningún mantenimiento';

                        } 
                        else {
                        return 'La furgoneta necesita de mantenimiento';
                        }
                      },
        ],
            
            [
                    'attribute' => 'ubicacion',
                    'label' => 'Nave Origen',
                    'value' => $model->codigoNave['ubicacion']
                      
                    
                ],
            
        ],
    ]) ?>

</div>
