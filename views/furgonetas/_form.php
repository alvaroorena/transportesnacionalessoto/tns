<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Furgonetas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="furgonetas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'matricula')-> label ('Matrícula')->textInput(['placeholder' => 'Ejemplo: 5805GTZ', 'maxlength' => true]) ?>

    <?php
    
    $tipos = 
            [
             'Citroën Berlingo' => 'Citroën Berlingo',
             'Citroën Jumper' => 'Citroën Jumper',
             'Dacia Dokker' => 'Dacia Dokker',
             'Fiat Ducato' => 'Fiat Ducato',
             'Ford Transit' => 'Ford Transit',
             'Mercedes Viano' => 'Mercedes Viano',
             'Mercedes Vito' => 'Mercedes Vito',
             'Opel Combi' => 'Opel Combi',
             'Peugeot Boxer' => 'Peugeot Boxer',
             'Renault Dokker' => 'Renault Dokker',
             'Renault Master' => 'Renault Master',
             'Renault Trafic' => 'Renault Trafic',
             'Volkswagen Transporter' => 'Volkswagen Transporter',
             
                
             
                
            ];
    
            echo $form->field($model, 'modelo_completo')
               ->dropDownList($tipos,['prompt' => 'Seleccione una furgoneta'])
               -> label ('Seleccionar Furgoneta');
    ?>

    <?= $form->field($model, 'kilometros')-> label ('Kilómetros')->textInput(['placeholder' => 'Ejemplo : 145765']) ?>

     <?= $form->field($model, 'motor')->textInput(['placeholder' => ' Ejemplo: 95cv']) ?>

    <?php
    
    $tipos = 
            [
             'Disponible' => 'Disponible',
             'Trabajando' => 'Trabajando',
                
             
                
            ];
    
            echo $form->field($model, 'estado')
               ->dropDownList($tipos,['prompt' => 'Seleccione un estado'])
               -> label ('Seleccionar Estado');
    ?>

     <?= $form->field($model, 'consumo')->textInput (['placeholder' => 'Ejemplo : 9.3']) ?>
    

    <?php
       $mapnaves = ArrayHelper::map($modelnaves, 'codigo_nave', 'ubicacion');
               echo $form->field($model, 'codigo_nave')
               ->dropDownList($mapnaves,['prompt' => 'Seleccione una Nave'])
               -> label ('Seleccionar Nave');
        
        ?>
    <?php
    
    $revision = 
            [
             '1' => 'Mantenimiento Realizado',
             '0' => 'Mantenimiento no Realizado',
             
             
             
                
            ];
    
            echo $form->field($model, 'revision')
               ->dropDownList($revision,['prompt' => 'Seleccione si la furgoneta necesita de algún tipo de revisión'])
               -> label ('Mantenimiento');
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
