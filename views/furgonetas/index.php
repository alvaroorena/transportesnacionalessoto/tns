<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'FURGONETAS';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="furgonetas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar Furgoneta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

//            'codigo_furgoneta',
            'matricula',
            'modelo_completo',
            'kilometros',
            'motor',
            //'revision',
            //'estado',
            //'consumo',
            //'codigo_nave',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
